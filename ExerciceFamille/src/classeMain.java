public class classeMain {
	public static void main(String[] args) {
		
		Famille remy = new Famille("Robin", "R�my", 22, 'M', 1);
		Famille alain = new Famille("Robin", "Alain", 53, 'M', 2);
		Famille faty = new Famille("Robin", "Faty", 52, 'F', 3);
		Famille maxime = new Famille("Ihuellou", "Maxime", 19, 'M', 4);
		Famille lydie = new Famille("Guizelin", "Lydie", 30, 'F', 5);
		Famille thomas = new Famille("Guizelin", "Thomas", 29, 'M', 6);
		
		System.out.println("La Famille ROBIN\n**********************\n");
		System.out.println("Voici " + remy.getNom() + " " + remy.getPrenom() + ", ag� de " + remy.getAge() + " de sexe " + remy.getSexe() + " // ID = " + remy.getID()+ "\n");
		System.out.println("Voici " + alain.getNom() + " " + alain.getPrenom() + ", ag� de " + alain.getAge() + " de sexe " + alain.getSexe() + " // ID = " + alain.getID()+ "\n");
		System.out.println("Voici " + faty.getNom() + " " + faty.getPrenom() + ", ag� de " + faty.getAge() + " de sexe " + faty.getSexe() + " // ID = " + faty.getID()+ "\n");
		System.out.println("Voici " + maxime.getNom() + " " + maxime.getPrenom() + ", ag� de " + maxime.getAge() + " de sexe " + maxime.getSexe() + " // ID = " + maxime.getID()+ "\n");
		System.out.println("Voici " + thomas.getNom() + " " + thomas.getPrenom() + ", ag� de " + thomas.getAge() + " de sexe " + thomas.getSexe() + " // ID = " + thomas.getID()+ "\n");
		System.out.println("Voici " + lydie.getNom() + " " + lydie.getPrenom() + ", ag� de " + lydie.getAge() + " de sexe " + lydie.getSexe() + " // ID = " + lydie.getID()+ "\n");
		System.out.println(remy.comparerSexe(maxime));
		System.out.println(remy.comparerAge(maxime));
		System.out.println(remy.comparerCouple(maxime) + "\n");
		System.out.println(alain.comparerSexe(faty));
		System.out.println(alain.comparerAge(faty));
		System.out.println(alain.comparerCouple(faty) + "\n");
		System.out.println(alain.comparerSexe(lydie));
		System.out.println(alain.comparerAge(lydie));
		System.out.println(alain.comparerCouple(lydie)+ "\n");
		System.out.println("Il y a " + remy.getNbreMembre() + " membres dans la famille.");

		}
}