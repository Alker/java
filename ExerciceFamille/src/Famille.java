
public class Famille {
	
		private String nom, prenom;
		private int age, id;
		private char sexe;
		private static int nbrMembre;
		
		
		public Famille() {
			nbrMembre++;
			nom = "Inconnu";
			prenom = "Inconnu";
			age = 0;
			sexe = 'M';
			id = 0;
		}
		
		public Famille(String n, String p, int a, char s, int i) {
			nbrMembre++;
			this.setNom(n);
			this.setPrenom(p);
			this.setAge(a);
			this.setSexe(s);
			this.setID(i);
			
		}
		
		public String getNom() {
			return this.nom;
		}
		
		public String getPrenom() {
			return this.prenom;
		}
		
		public int getAge() {
			return this.age;
		}
		
		public char getSexe() {
			return this.sexe;
		}
		public int getID() {
			return this.id;
		}
		
		public int getNbreMembre() {
			return nbrMembre;
		}
		
		public void setNom(String n) {
			this.nom = n;
		}
		
		public void setPrenom(String p) {
			this.prenom = p;
		}
		
		public void setAge(int a) {
			this.age = a;
		}
		
		public void setSexe(char s) {
			this.sexe = s;
		}
		public void setID(int i) {
			this.id = i;
		}
		
		public String comparerAge(Famille f) {
			String str = new String();
			
			if (f.getAge() > this.age)
				str = f.getPrenom() + " est plus ag�(e) que " + this.prenom;
			else 
				str = this.prenom + " est plus ag�(e) que " + f.getPrenom();
			return str;
		}
		public String comparerSexe(Famille f) {
			String str = new String();
			
			if (f.getSexe() != this.sexe)
				str = f.getPrenom() + " et " + this.prenom + " ne sont pas de m�me sexe.";
			else
				str = f.getPrenom() + " et " + this.prenom + " sont de m�me sexe.";
			return str;
		}
		public String comparerCouple(Famille f) {
			String str = new String();
			
			if (f.getID() == 1 && this.id == 4 || f.getID() == 4 && this.id == 1)
				str = f.getPrenom() + " et " + this.prenom + " sont en couple. C'est les deux loup les plus sexy du monde. <3 <3";
			else if (f.getID() == 2 && this.id == 3 || f.getID() == 3 && this.id == 2)
				str = f.getPrenom() + " et " + this.prenom + " sont en couple.";
			else
				str = f.getPrenom() + " et " + this.prenom + " ne sont pas en couple.";
			return str;
		}
		

}
