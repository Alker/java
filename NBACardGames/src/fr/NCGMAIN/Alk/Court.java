package fr.NCGMAIN.Alk;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.ListIterator;

public class Court {
	
	String name, town;
	int capacity;
	
	
	public Court() {
		this.name = null;
		this.capacity = 0;
		this.town = null;
	}
	

	public Court(String name,int cap,String town) {
		this.name = name;
		this.capacity = cap;
		this.town = town;
	}
	
//GETTER
	public String getName() {
		return this.name;
	}

	public String getTown() {
		return this.town;
	}
	
	public int getCap() {
	return this.capacity;
	}
	
//SETTER
	public void setName(String name) {
		this.name = name;
	}

	public void setCapacity(int cap) {
		this.capacity = cap;
	}

	public void setTown(String town) {
		this.town = town;
	}
	
	final static String[] courtName = {"State Farm Arena", "TD Garden", "Barclays Center", "Spectrum Center", "United Center", "Quicken Loans Arena", "American Airlines Center", "Pepsi Center", "Little Caesars Arena", "Chase Center" ,"Toyota Center", "Baankers Life Fieldhouse", "Staples Center", "FedEx Forum", "American Airlines Arena", "Fiserv Forum", "Target Center", "Smoothie King Center", "Madison Square Garden", "Chesapeake Energy Arena", "Amway Center", "Wells Fargo Center", "Talking Stick Resort Arena", "Moda Center", "Golden 1 Center", "AT&T Center", "Scotiabank Arena", "Vivint Smart Home Arena", "Capital One Arena","Staples Center"};
	final static int[] capacityC = {21000, 20000, 17700, 20200, 20900, 20600, 19200, 19200, 21000, 18000, 18300, 18300, 19000, 18200, 19600, 17500, 19400, 18500, 19000, 18200, 20000, 20300, 18400, 20500, 17500, 18400, 19800, 19900, 20300, 19000};
	final static String[] townName = {"Atlanta", "Boston", "Brooklyn", "Charlotte", "Chicago", "Cleveland", "Dallas", "Denver", "Detroit" ,"Golden States", "Houston", "Indiana", "Los Angeles", "Memphis", "Miami", "Milwaukee", "Minnesota", "New Orleans", "New York", "Oklahoma City", "Orlando", "Philadelphia", "Phoenix", "Portland", "Sacramento", "San Antonio", "Toronto", "Utah", "Washington","Los Angeles"};


//Initialise les terrain,leur leur capacit�
	public static List InitC() {
		
		List court = new ArrayList();
		
		for (int i = 0; i < courtName.length;i++) {
			court.add(new Court(courtName[i],capacityC[i],townName[i]));
		}
		return court;
	}
	
	public static void testInitC() {
		List court = InitC();
		Court test = (Court) court.get(4);
		if (test.getName() != "United Center")
			System.out.println("TEST ECHOUE, MAUVAIS NOM SORTI, != United Center");
		else
			System.out.println("TEST OK");
	}
	
	/*Donne le terrain par rapport au nom d'une �quipe
	 * @since 0.10.1
	 * @param s Nom de l'�quipe
	 * @return result terrain de l'�quipe choisi
	 */
	public static String CourtByName(String s) {
		String result = " ";
		switch (s) {
		
		case "Hawks":
			result = courtName[0];
		break;

		case "Celtics":
			result = courtName[1];
		break;

		case "Nets":
			result = courtName[2];
		break;
		
		case "Hornets":
			result = courtName[3];
		break;

		case "Bulls":
			result = courtName[4];
		break;

		case "Cavaliers":
			result = courtName[5];
		break;

		case "Mavericks":
			result = courtName[6];
		break;

		case "Nuggets":
			result = courtName[7];
		break;

		case "Pistons":
			result = courtName[8];
		break;

		case "Warriors":
			result = courtName[9];
		break;
		
		case "Rockets":
			result = courtName[10];
		break;

		case "Pacers":
			result = courtName[11];
		break;

		case "Lakers":
			result = courtName[12];
		break;

		case "Grizzles":
			result = courtName[13];
		break;
		
		case "Heat":
			result = courtName[14];
		break;

		case "Bucks":
			result = courtName[15];
		break;
		
		case "Timberwolves":
			result = courtName[16];
		break;
		
		case "Pelicans":
			result = courtName[17];
		break;

		case "Knicks":
			result = courtName[18];
		break;
		
		case "Thunders":
			result = courtName[19];
		break;

		case "Magic":
			result = courtName[20];
		break;

		case "Sixers":
			result = courtName[21];
		break;
		
		case "Suns":
			result = courtName[22];
		break;
		
		case "Trail":
			result = courtName[23];
		break;
		
		case "Kings":
			result = courtName[24];
		break;

		case "Spurs":
			result = courtName[25];
		break;
		
		case "Raptor":
			result = courtName[26];
		break;

		case "Jazz":
			result = courtName[27];
		break;
		
		case "Wizards":
			result = courtName[28];
		break;

		case "Clippers":
			result = courtName[29];
		break;
		}
		return result;
	}
	
}
