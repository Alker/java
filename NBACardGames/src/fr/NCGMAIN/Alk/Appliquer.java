package fr.NCGMAIN.Alk;
/*Applique le nom de l'�quipe par rapport � un identifiant choisi
 * @since 0.05.1
 * @param i ID choisi par l'utilisateur
 * @return rep Nom de l'�quipe par rapport � l'ID
 */
public class Appliquer {
	public static String Applique(int i) {
		String rep = " ";
		switch(i) {
		case 0 :
			rep = "Hawks";
		break;

		case 1 :
			rep = "Celtics";
		break;

		case 2 :
			rep = "Nets";
		break;
		
		case 3 :
			rep = "Hornets";
		break;

		case 4 :
			rep = "Bulls";
		break;

		case 5 :
			rep = "Cavaliers";
		break;

		case 6 :
			rep = "Mavericks";
		break;

		case 7 :
			rep = "Nuggets";
		break;

		case 8 :
			rep = "Pistons";
		break;

		case 9 :
			rep = "Warriors";
		break;
		
		case 10 :
			rep = "Rockets";
		break;

		case 11 :
			rep = "Pacers";
		break;

		case 12 :
			rep = "Lakers";
		break;

		case 13 :
			rep = "Grizzles";
		break;
		
		case 14 :
			rep = "Heat";
		break;

		case 15 :
			rep = "Bucks";
		break;
		
		case 16 :
			rep = "Timberwolves";
		break;
		
		case 17 :
			rep = "Pelicans";
		break;

		case 18 :
			rep = "Knicks";
		break;
		
		case 19 :
			rep = "Thunders";
		break;

		case 20 :
			rep = "Magic";
		break;

		case 21 :
			rep = "Sixers";
		break;
		
		case 22:
			rep = "Suns";
		break;
		
		case 23 :
			rep = "Trail";
		break;
		
		case 24 :
			rep = "Kings";
		break;

		case 25 :
			rep = "Spurs";
		break;
		case 26 :
			rep = "Raptor";
		break;

		case 27 :
			rep = "Jazz";
		break;
		
		case 28 :
			rep = "Wizards";
		break;

		case 29 :
			rep = "Clippers";
		break;
		}
		return rep;
	}
}
