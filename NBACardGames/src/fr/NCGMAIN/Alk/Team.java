package fr.NCGMAIN.Alk;

import java.util.ArrayList;
import java.util.List;

public class Team {
	String name,town,conf,court;



	public Team() {
		this.name = null;
		this.town = null;
		this.conf = null;
		this.court = null;
	}

	public Team(String name, String town, String conf, String court) {
		this.name = name;
		this.town = town;
		this.conf = conf;
		this.court = court;
	}

	public String getName() {
		return this.name;
	}
	public String getTown() {
		return this.town;
	}
	public String getConf() {
		return this.conf;
	}
	public String getCourt() {
		return this.court;
	}
								  //  0         1         2        3         4          5             6          7          8          9           10        11        12          13        14       15           16           17         18         19        20        21       22         23            24       25        26       27        28         29
	final static String[] names = {"Hawks", "Celtics", "Nets", "Hornets", "Bulls", "Cavaliers", "Mavericks", "Nuggets", "Pistons", "Warriors" ,"Rockets", "Pacers", "Lakers", "Grizzlies", "Heat", "Bucks", "Timberwolves", "Pelicans", "Knicks", "Thunder", "Magics", "76ers", "Suns", "Trail Blazers", "Kings", "Spurs", "Raptors", "Jazz", "Wizards", "Clippers"};

	public static List InitT() {
						List team = new ArrayList();
		int i = 0;
				
		for (i = 0; i < names.length;i++) {
			Court c = (Court) Court.InitC().get(i);
			//System.out.println(c.getName());
			if(i == 0 || i == 1 || i == 2 || i == 3 || i == 4 || i == 5 || i == 8 || i == 11 || i == 14 || i == 15 || i == 18 || i == 20 || i == 21 || i == 26 || i == 28)
				team.add(new Team(names[i], c.getTown(), "East", c.getName()));
			else { 
				team.add(new Team(names[i], c.getTown(), "West", c.getName()));
				}
			//System.out.println(((Team) team.get(i)).getName() + " == " + ((Team) team.get(i)).getTown() + " == " + ((Team) team.get(i)).getConf());
			}
		return team;
	}
	
	public static String getNames(int i) {
		return names[i];
	}
	
	public static void testInitT() {
		List team = InitT();
		Team lakers = (Team) team.get(4);
		if (lakers.getName() != "Bulls")
			System.out.println("TEST ECHOUE, MAUVAIS NOM SORTI");
		else
			System.out.println("TEST OK");
	}
	
	/*Donne le terrain par rapport au nom d'une �quipe
	 * @since 0.10.1
	 * @param s Nom de l'�quipe
	 * @return result terrain de l'�quipe choisi
	 */
	public static String TeamByName(String s) {
		String result = " ";
		switch (s) {
		
		case "Hawks":
			result = names[0];
		break;

		case "Celtics":
			result = names[1];
		break;

		case "Nets":
			result = names[2];
		break;
		
		case "Hornets":
			result = names[3];
		break;

		case "Bulls":
			result = names[4];
		break;

		case "Cavaliers":
			result = names[5];
		break;

		case "Mavericks":
			result = names[6];
		break;

		case "Nuggets":
			result = names[7];
		break;

		case "Pistons":
			result = names[8];
		break;

		case "Warriors":
			result = names[9];
		break;
		
		case "Rockets":
			result = names[10];
		break;

		case "Pacers":
			result = names[11];
		break;

		case "Lakers":
			result = names[12];
		break;

		case "Grizzles":
			result = names[13];
		break;
		
		case "Heat":
			result = names[14];
		break;

		case "Bucks":
			result = names[15];
		break;
		
		case "Timberwolves":
			result = names[16];
		break;
		
		case "Pelicans":
			result = names[17];
		break;

		case "Knicks":
			result = names[18];
		break;
		
		case "Thunders":
			result = names[19];
		break;

		case "Magic":
			result = names[20];
		break;

		case "Sixers":
			result = names[21];
		break;
		
		case "Suns":
			result = names[22];
		break;
		
		case "Trail":
			result = names[23];
		break;
		
		case "Kings":
			result = names[24];
		break;

		case "Spurs":
			result = names[25];
		break;
		
		case "Raptor":
			result = names[26];
		break;

		case "Jazz":
			result = names[27];
		break;
		
		case "Wizards":
			result = names[28];
		break;

		case "Clippers":
			result = names[29];
		break;
		}
		return result;
	}
}
