package fr.NCGMAIN.Alk;

import java.util.Random;

public class Randoms {
/*Prend un nom d'�quipe au hasard
 * @since 0.1
 * @return val nom de team
 */
	public static String RandomTeam() {
		String[] team = {"Bulls","Hawks","Celtics","Hornets","Heat","Nets","Cavaliers","Knicks","Pistons","Magic","Sixers","Pacers","Wizards","Raptor","Bucks","Mavericks","Nuggets","Warriors","Rockets","Timberwolves","Clippers","Grizzlies","Thunder","Lakers","Pelicans","Trail","Suns","Spurs","Jazz","Kings"};
		String val = team[(int)(Math.random()*team.length)];
		return val;
	}
	
/*Prend un nom de ville au hasard
* @since 0.1
* @return val nom de ville
*/	
	public static String RandomTown() {
		String[] town = {"Atlanta","Boston","Chicago","Charlotte","Brooklyn","Cleveland","Miami","New York","Detroit","Orlando","Philadelphie","Indiana","Washington","Toronto","Milwaukee","Dallas","Denver","Golden States","Houston","Minnesota","Los Angeles","Memphis","Oklahoma City","Los Angeles","Nouvelle Orleans","Portland","Phoenix","San Antonio","Utah","Sacramento"};
		String val = town[(int)(Math.random()*town.length)];
		return val;
	}
	
	/*Prend un joueur au hasard
	 * @since 0.1
	 * @return un joueur
	 */
	public static String RandomJoueur() {
		String[] player = {"MJ23","KB24","DW03","LJ23","SO34","LB33","SC30","VC15","AD03","CB04"};
		String val = player[(int)(Math.random()*player.length)];
		return val;
	}
	/*Prend une stat au hasard pour l'opposition du quart temps 
	 * @since 0.1
	 * @return un libell� de stat
	 */
	public static String RandomComp() {
		String[] competence = {"ATT","DEF","REB","ORG"};
		String val = competence[(int)(Math.random()*competence.length)];
		return val;
	}
/*Valeur al�atoire entre 0 et 50 puis 50 ajouter � celle ci
 * @since 0.25.2
 * @return valeur entre 50 et 99
 */
	public static int Randomstat() {
		int i = 50;
		Random random = new Random();
		int val;
		val = random.nextInt(50);
		val += i;
		return val;
	}
	
}
