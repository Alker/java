package fr.NCGMAIN.Alk;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class Save {
	
	public static void Sauvegarde(String test) {
		Scanner sc = new Scanner(System.in);
		String nom;
		System.out.println("Donnez un nom � votre sauvegarde : ");
		nom = sc.next();
		Path save = Paths.get(nom+".txt");
		//System.out.println("Nom du fichier : " + save.getFileName());
		//System.out.println("Est-ce que la sauvegarde existe ? " + Files.exists(save));
		//System.out.println("Chemin absolu du fichier : " + save.toAbsolutePath());
		 try{
		PrintWriter ecri = new PrintWriter(new FileWriter(nom+".txt"));
		ecri.print(test + System.getProperty("line.separator"));
		ecri.print(test);
		ecri.flush();
		ecri.close();
		 } catch (java.io.IOException e ) {e.printStackTrace();}
	}
	
	public static void SaveGame(Game game) {
		Scanner sc = new Scanner(System.in);
		String nom;
		System.out.println("Donnez un nom � votre sauvegarde : ");
		nom = sc.next();
		Path save = Paths.get(nom+".txt");
		try {
			PrintWriter ecri = new PrintWriter(new FileWriter("Save"+nom+".txt"));
			ecri.print("Nom :" + game.getTeamP1() + System.getProperty("line.separator"));
			ecri.print("Ville :" + game.getTownP1() + System.getProperty("line.separator"));
			ecri.print("Terrain :" + game.getCourtP1() + System.getProperty("line.separator"));
			for(int i = 0; i< game.getTeamEffP1().length;i++)
			ecri.print("Joueur :" + game.getTeamEffIP1(i) + System.getProperty("line.separator"));
			ecri.flush();
			ecri.close();
		} catch (java.io.IOException e ) {e.printStackTrace();}
	}
}
