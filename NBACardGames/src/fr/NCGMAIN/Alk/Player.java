package fr.NCGMAIN.Alk;

public class Player {

	protected String fName, lName;
	protected String team;
	protected String pos;
	protected String pos2;
	protected String id;
	protected int gen, att, def, org, reb,number;

	public Player() {
		this.fName = null;
		this.lName = null;
		this.team = null;
		this.pos = null;
		this.pos2 = null;
		this.gen = 0;
		this.att = 0;
		this.def = 0;
		this.org = 0;
		this.reb = 0;
		this.number = 0;
		this.id = null;
	}

	public Player(String fName, String lName, String team, String pos1, String pos2, int gen, int att, int def, int org, int reb, int number, String id) {
		this.fName = fName;
		this.lName = lName;
		this.team = team;
		this.pos = pos1;
		this.pos2 = pos2;
		this.gen = gen;
		this.att = att;
		this.def = def;
		this.org = org;
		this.reb = reb;
		this.number = number;
		this.id = id;
	}

	public String getPrenom() {
		return this.lName;
	}

	public String getNom() {
		return this.fName;
	}

	public String getTeam() {
		return this.team;
	}

	public String getPoste1() {
		switch(this.pos) {

		case "SG":
			this.pos = "A";
			break;
		case "PG":
			this.pos = "MJ";
			break;
		case "SF":
			this.pos = "AL";
			break;
		case "PF":
			this.pos = "AF";
			break;
		case "C":
			this.pos = "P";
			break;
		}
		return this.pos;
	}
	public String getPoste2() {
		switch(this.pos2) {

		case "SG":
			this.pos2 = "A";
			break;
		case "PG":
			this.pos2 = "MJ";
			break;
		case "SF":
			this.pos2 = "AL";
			break;
		case "PF":
			this.pos2 = "AF";
			break;
		case "C":
			this.pos2 = "P";
			break;
		}
		return this.pos2;
	}
	public int getGEN() {
		return this.gen;
	}

	public int getATT() {
		return this.att;
	}

	public int getDEF() {
		return this.def;
	}

	public int getREB() {
		return this.reb;
	}

	public int getORG() {
		return this.org;
	}

	public int getNumber() {
		return this.number;
	}
	
	public String getId() {
		return this.id;
	}
	
	public String getRarity() {
		String rar;
		if (gen<70)
			rar = "BRONZE";
		else if (gen>=70&&gen<76)
			rar = "ARGENT";
		else if (gen>=76&&gen<80)
			rar ="OR";
		else if (gen>=80&&gen<84)
			rar ="EMERAUDE";
		else if (gen>=84&&gen<87)
			rar ="SAPHIR";
		else if (gen>=87&&gen<90)
			rar ="RUBIS";
		else if (gen>=90&&gen<94)
			rar ="AMETHYSTE";
		else if (gen>=94&&gen<96)
			rar ="DIAMANT";
		else if (gen>=96&&gen<99)
			rar ="DIAMANT ROSE";
		else
			rar = "OPALE GALAXY";
		return rar;
	}
	
	public void setPrenom(String lName) {
		this.lName = lName;
	}
	
	public void setNom(String fName) {
		this.fName = fName;
	}

	public void setTeam(String team) {
		this.team = team;
	}
	
	public void setPos1(String pos1) {
		this.pos = pos1;
	}
	
	public void setPos2(String pos2) {
		this.pos2 = pos2;
	}
	
	public void setGEN(int gen) {
		this.gen = gen;
	}

	public void setATT(int att) {
		this.att = att;
	}

	public void setDEF(int def) {
		this.def = def;
	}

	public void setREB(int reb) {
		this.reb = reb;
	}

	public void setORG(int org) {
		this.org = org;
	}

	public void setnumber(int numb) {
		this.number = numb;
	}
	
	public void setId(String i) {
		this.id = i;
	}
	

	
/*Cr�� le joueur demander
 * @since 0.0.1
 * @param pl id du joueur
 * @return un joueur
 */
	public static Player Players(String pl) {
		Player rep = new Player("vide","vide","vide","vide","vide",0,0,0,0,0,0,"vide");
		switch (pl) {
		
		case "MJ23" :
			rep = new Player("Jordan", "Michael" ,Team.getNames(4), "SG", "SF",99,97,97,79,92,23,"MJ23");;
			break;
			
		case "KB24" :
			rep = new Player("Bryant", "Kobe" ,Team.getNames(23), "SF", "SG",99,99,89,99,95,24,"KB24");
			break;
			
		case "DW03" :
			rep = new Player("Wade", "Dwyane" ,Team.getNames(6), "SG", "PG",99,99,99,97,95,3,"DW03");
			break;
			
		case "LJ23" :
			rep = new Player("James", "Lebron" ,Team.getNames(23), "PG", "SF",97,93,91,79,91,23,"LJ23"); 
			break;
			
		case "SO34" :
			rep = new Player("O'neil", "Shaquille" ,Team.getNames(23), "C", "PF",98,90,92,97,63,34,"SO34");
			break;
			
		case "LB33" :
			rep = new Player("Bird", "Larry" ,Team.getNames(1), "PF", "SF",99,94,90,93,90,33,"LB33");
			break;
			
		case "SC30" :
			rep = new Player("Curry", "Stephen" ,Team.getNames(17), "PG", "SG",99,94,91,70,98,30,"SC30");
			break;
			
		case "VC15" :
			rep = new Player("Carter", "Vince" ,Team.getNames(13), "SG", "SF",99,95,89,66,91,15,"VC15");
			break;
			
		case "AD03" :
			rep = new Player("Davis", "Anthony" ,Team.getNames(23), "PF", "C",99,99,97,99,92,3,"AD03");
			break;
			
		case "CB04" :
			rep = new Player("Bosh", "Chris" ,Team.getNames(6), "C", "PF",97,90,84,96,60,4,"CB04");
			break;
			
		default :
			final Player rien = new Player("vide","vide","vide","vide","vide",0,0,0,0,0,0,"vide");
			break;
		}
		return rep;
	}
}

