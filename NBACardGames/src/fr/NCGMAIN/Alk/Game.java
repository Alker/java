package fr.NCGMAIN.Alk;

import java.util.Scanner;
import java.util.HashSet;
import java.util.Random;


public class Game {
	static String TownP1;
	static String TeamP1;
	static String CourtP1;
	static String[] TeamEffP1 = {" "," "," "," "," "};
	static String TownAI;
	static String TeamAI;
	static String CourtAI;
	static String[] TeamEffAI = {" "," "," "," "," "};

	public Game() {
		TownP1 = null;
		TeamP1 = null;
		CourtP1 = null;
		TeamEffP1 = null;
		TownAI = null;
		TeamAI = null;
		CourtAI = null;
		TeamEffAI = null;
	}

	public Game(String TWP, String TMP,String CTP,String[] TP,String TWAI, String TMAI,String CTAI,String[] TAI) {
		TownP1 = TWP;
		TeamP1 = TMP;
		CourtP1 = CTP;
		TeamEffP1 = TP;
		TownAI = TWAI;
		TeamAI = TMAI;
		CourtAI = CTAI;
		TeamEffAI = TAI;
	}

	public void setTownP1(String TP) {
		TownP1 = TP;
	}

	public void setTeamP1(String TP) {
		TeamP1 = TP;
	}

	public void setCourtP1(String CT) {
		CourtP1 = CT;
	}
	public void setTeamEffP1(String[] T) {
		TeamEffP1 = T;
	}

	public void setTownAI(String TP) {
		TownAI = TP;
	}

	public void setTeamAI(String TP) {
		TeamAI = TP;
	}

	public void setCourtAI(String CT) {
		CourtAI = CT;
	}
	public void setTeamEffAI(String[] T) {
		TeamEffAI = T;
	}

	public String getTownP1() {
		return TownP1;
	}

	public String getTeamP1() {
		return TeamP1;
	}
	public String getCourtP1() {
		return CourtP1;
	}

	public String[] getTeamEffP1() {
		return TeamEffP1;
	}

	public String getTeamEffIP1(int i) {
		return TeamEffP1[i];
	}

	public String getTownAI() {
		return TownAI;
	}

	public String getTeamAI() {
		return TeamAI;
	}
	public String getCourtAI() {
		return CourtAI;
	}

	public String[] getTeamEffAI() {
		return TeamEffAI;
	}

	public String getTeamEffIAI(int i) {
		return TeamEffAI[i];
	}
	/*Cr�� l'�quipe du Joueur
	 * @since 0.0.1
	 * @exception InterruptedException Si le timer fait planter le programme
	 */
	public static void Play() throws InterruptedException {
		int time = 1000;
		String[][] Pk ={{" "," "," "," "," "},
						   {" "," "," "," "," "}};
		Game game = new Game("rien","rien","rien",Pk[0],"rien","rien","rien",Pk[1]);
		game.setTownP1(game.getTownP1());
		Scanner sc = new Scanner(System.in);
		char rep4 = ' ';
		do {
			switch(game.getTeamP1()) {
			case "rien":
				System.out.println("Nous allons cr�er une �quipe");
				rep4 = 'O';
				break;
			default:

				rep4 = sc.nextLine().charAt(0);
				break;
			}while(rep4!='O'&&rep4!= 'T'&&rep4!='C');
			switch(rep4) {
			case 'O':
				System.out.println("\nVotre �quipe repr�sentera quelle ville ? ");
				game.setTownP1(sc.nextLine());
				System.out.println("\nSuper !! Quel sera le nom de votre �quipe ? ");
				game.setTeamP1(sc.nextLine());
				System.out.println("\nVoici donc les " + game.getTownP1() + " " + game.getTeamP1() + " !!!!!");
				System.out.println("\nVoici un packs Terrain gratuit rien que pour vous !!!");
				System.out.println("*****************************");
				System.out.println("Votre nouveau terrain est le..... ");
				Thread.sleep(time);
				String TmCrtP1=Appliquer.Applique((int)(Math.random()*(29)));
				String Courts=Court.CourtByName(TmCrtP1);
				game.setCourtP1(Courts);
				System.out.println(Courts + " des " + Team.TeamByName(TmCrtP1)+"\n");
				Thread.sleep(1000);
				game.setTeamEffP1(openPack());
				break;
			case 'T':
				game.setTeamEffP1(openPack());
				break;
			case 'C':
				Save.SaveGame(game);
				PlayingAI(game);
				break;
			}
			if(rep4!='C')
				System.out.println("Voulez vous recr�er une �quipe (O), refaire un tirage de pack(T), ou continuer (C).");
		}while(rep4 == 'O'|| rep4 =='T');

	}
	/*Simule le tirage au sort de 5 joueur pour le joueur
	 * @since 0.0.5
	 *  @exception InterruptedException Si le timer fait planter le programme
	 *  @return StrP1 �quipe de P1 compl�te
	 */
	public static String[] openPack() throws InterruptedException {
		Scanner sg = new Scanner(System.in);
		System.out.println("Choississez maintenant votre Pack Joueur !!");
		System.out.println("\"Jordan\", \"Bryant\", \"Carter\"");
		String c = sg.nextLine();
		HashSet<String> PlayerP1 = new HashSet<String>();
		switch(c) {
		case "Jordan":
			PlayerP1.add("MJ23");
			do{
				PlayerP1.add(Randoms.RandomJoueur());
			}while(PlayerP1.size()<5);
			break;

		case "Bryant":
			PlayerP1.add("KB24");
			do{
				PlayerP1.add(Randoms.RandomJoueur());
			}while(PlayerP1.size()<5);
			break;

		case "Carter":
			PlayerP1.add("VC15");
			do{
				PlayerP1.add(Randoms.RandomJoueur());
			}while(PlayerP1.size() <5);
			break;}
		System.out.println("\n\t\tPack de Joueur " + c);
		System.out.println("------------------------------------------------------");

		Object[] obj = PlayerP1.toArray();
		String[] StrP1 = {" "," "," "," "," "};
		int j = 1;
		for(int i = 0; i < obj.length; i++) {
			StrP1[i] = obj[i].toString();
			System.out.println("Carte N�"+ j +" => " + Player.Players(StrP1[i]).getPrenom() + "\t" + Player.Players(StrP1[i]).getNom() + "\tRaret� : " + Player.Players(StrP1[i]).getRarity());
			j++;}
		Thread.sleep(1000);
		return StrP1;
	}
	/*Applique une �quipe � l'ordinateur et d�roule le match
	 * @since 0.10.1
	 * @param Game partie en cours
	 * @exception InterruptedException Si le timer fait planter le programme
	 */
	public static void PlayingAI(Game games) throws InterruptedException {
		Scanner sc = new Scanner (System.in);
		HashSet<String> PlayerAI = new HashSet<String>();
		char rejoue = ' ';
		do {
			rejoue = 'I';
			do{
				PlayerAI.add(Randoms.RandomJoueur());
			}while(PlayerAI.size()<5);
			Object[] objAI = PlayerAI.toArray();
			String[] StrAI = {" "," "," "," "," "};
			int j = 1;
			System.out.println("\n\t\tJoueur de l'IA");
			System.out.println("------------------------------------------------------");
			for(int i = 0; i < objAI.length; i++) {
				StrAI[i] = objAI[i].toString();
				System.out.println("Carte N�"+ j +" => " + Player.Players(StrAI[i]).getPrenom() + "\t" + Player.Players(StrAI[i]).getNom() + "\tRaret� : " + Player.Players(StrAI[i]).getRarity());
				j++;}
			Thread.sleep(1000);
			games.setTeamEffAI(StrAI);
			games.setTeamAI(Randoms.RandomTeam());
			games.setTownAI(Randoms.RandomTown());
			System.out.print("\nL'adversaire sera :" +games.getTownAI() + " " + games.getTeamAI() +" et joueront sur le ");
			String CourtAI=Court.CourtByName(games.getTeamAI());
			games.setCourtAI(CourtAI);
			System.out.println(games.getCourtAI() + " des " + games.getTeamAI() +".");
			Thread.sleep(1000);
			System.out.println("\nBienvenue dans ce match NBA !\n");
			PFMATCH(games);
			System.out.println("\nVoici les " + games.getTownP1() + " " +games.getTeamP1() + ". L'�quipe est constituer de : ");
			int moyGenP1=0,moyGenAI=0;
			for (int i =0;i<games.getTeamEffP1().length;i++) {
				System.out.println(Player.Players(games.getTeamEffIP1(i)).getNom() + "\t" + Player.Players(games.getTeamEffIP1(i)).getGEN());
				moyGenP1 = (moyGenP1 + Player.Players(games.getTeamEffIP1(i)).getGEN());
				moyGenAI = (moyGenAI + Player.Players(games.getTeamEffIAI(i)).getGEN());
			}
			Thread.sleep(2000);
			System.out.println("\nOppos� aux : " + games.getTownAI() + " " + games.getTeamAI() + ". Compos� de : ");
			for (int i =0;i<games.getTeamEffAI().length;i++) {
				System.out.println(Player.Players(games.getTeamEffIAI(i)).getNom() + "\t" + Player.Players(games.getTeamEffIAI(i)).getGEN());}
			moyGenP1 = moyGenP1/5;
			moyGenAI = moyGenAI/5;
			Thread.sleep(1000);
			System.out.println("\nLes moyennes General des deux �quipe sont : "+ games.getTeamP1() + " : " + moyGenP1 + " " + games.getTeamAI() + " : " + moyGenAI);
			Quarter(games);
			do {
				System.out.println("Voulez vous rejouer ??? (O/N)");
				rejoue = sc.next().charAt(0);
			}while(rejoue !='O' && rejoue != 'N');
		}while(rejoue == 'O');
	}
	/* Choisi un terrain au hasard entre celui de P1 et de AI pour le d�roulement du match
	 * @since 0.10.1
	 * @param games Partie en cours
	 * @return result Terrain du match
	 */

	public static String PFMATCH(Game play) {
		Random random = new Random();
		String result;
		int pileOuFace = random.nextInt(2);
		if (pileOuFace == 0) {
			result =play.getCourtP1();
			System.out.println("Nous jouerons � domicile au " + result + " de " + play.getTownP1());
		} else {
			result=play.getCourtAI();
			System.out.println("Nous jouerons � l'exterieur au " + result + " de " + play.getTownAI());
		}
		return result;
	}
	/*Boucle qui d�roule les quart temps
	 * @since 0.1
	 * @param Games Partie en cours
	 */
	public static void Quarter(Game games) {
		Scanner sc = new Scanner (System.in);
		int[] Score = {0,0};
		int p = 1;
		int choix = 0,StatP1 = 0,StatAI=0;
		int u = 0;
		do {
			System.out.println("\nQuart-Temps "+ p +"!!!!");
			p++;
			System.out.println("\nSCORE : "+  Score[0] + "  < " + games.getTeamP1() +" :: "+ games.getTeamAI() +" > " + Score[1]);
			String comp = Randoms.RandomComp();
			String compIA = CompOpp(comp);
			System.out.println("\nChoisissez un joueur (" + comp + ")");
			for (int i =0;i<games.getTeamEffP1().length;i++) {
				System.out.println("(" + Player.Players(games.getTeamEffIP1(i)).getNom() + "\tEntrez le N� " + i + ") \tStat "+ comp + "=>\t" + Choix(comp,i,games));
			}
			choix = sc.nextInt();
			StatP1 = Choix(comp,choix,games);
			Random random = new Random();
			int j = random.nextInt(5);
			StatAI = statAI(compIA,j,games);
			Score = Score(Score,StatP1,StatAI);
			u++;
			System.out.println(Player.Players(games.getTeamEffIP1(choix)).getNom() + ", ("+ comp+ " " + Choix(comp,choix,games) + ") face � " + Player.Players(games.getTeamEffIAI(j)).getNom() + ", (" + compIA + " " + statAI(compIA,j,games)+")");
		}while(u<4);
		System.out.println("Fin du match !!! Score "+ Score[0] + " pour " + games.getTeamP1()+ " / " + Score[1] + " pour " + games.getTeamAI());
		if(Score[0]<Score[1])
			System.out.println("\nLes " + games.getTeamAI() + " gagne le match !!");
		else if (Score[0]>Score[1])
			System.out.println("Les " + games.getTeamP1() + " gagne le match !!");
		else {
			System.out.println("\nProlongation... Le suspense est � son comble... ");
			System.out.println("SCORE : "+  Score[0] + "  < " + games.getTeamP1() +" :: "+ games.getTeamAI() +" > " + Score[1]);
			String comp = Randoms.RandomComp();
			String compIA = CompOpp(comp);
			choix = 0;
			StatP1 = 0;
			StatAI=0;
			System.out.println("Choisissez un joueur (" + comp + ")");
			for (int i =0;i<games.getTeamEffP1().length;i++) {
				System.out.println("(" + Player.Players(games.getTeamEffIP1(i)).getNom() + "=>" + i + ") Stat "+ comp + "=>\t" + Choix(comp,i,games));
			}
			choix = sc.nextInt();
			StatP1 = Choix(comp,choix,games);
			Random random = new Random();
			int j = random.nextInt(5);
			StatAI = statAI(compIA,j,games);
			Score = Score(Score,StatP1,StatAI);
			System.out.println(Player.Players(games.getTeamEffIP1(choix)).getNom() + ", " + comp + " = " +Choix(comp,choix,games) + " face � " + Player.Players(games.getTeamEffIAI(j)).getNom() + ", "+ compIA + " = " + statAI(compIA,choix,games));
			System.out.println("SCORE : "+  Score[0] + "  < " + games.getTeamP1() +":"+ games.getTeamAI() +" > " + Score[1]);
			if(Score[0]<Score[1])
				System.out.println("\nLes " + games.getTeamAI() + " gagne le match apr�s des prolongations de folie!!");
			else if (Score[0]>Score[1])
				System.out.println("Les " + games.getTeamP1() + " gagne le match des suite d'un match � rallonge!!");
			else {
				System.out.println("\nProlongation... Le suspense est � son comble... ");
				System.out.println("SCORE : "+  Score[0] + "  < " + games.getTeamP1() +" :: "+ games.getTeamAI() +" > " + Score[1]);
				comp = Randoms.RandomComp();
				compIA = CompOpp(comp);
				choix = 0;
				StatP1 = 0;
				StatAI=0;
				System.out.println("Choisissez un joueur (" + comp + ")");
				for (int i =0;i<games.getTeamEffP1().length;i++) {
					System.out.println("(" + Player.Players(games.getTeamEffIP1(i)).getNom() + "=>" + i + ") Stat "+ comp + "=>\t" + Choix(comp,i,games));
				}
				choix = sc.nextInt();
				StatP1 = Choix(comp,choix,games);
				j = random.nextInt(5);
				StatAI = statAI(compIA,j,games);
				Score = Score(Score,StatP1,StatAI);
				System.out.println(Player.Players(games.getTeamEffIP1(choix)).getNom() + ", " + comp + " = " +Choix(comp,choix,games) + " face � " + Player.Players(games.getTeamEffIAI(j)).getNom() + ", "+ compIA + " = " + statAI(compIA,choix,games));
				System.out.println("SCORE : "+  Score[0] + "  < " + games.getTeamP1() +":"+ games.getTeamAI() +" > " + Score[1]);
				if(Score[0]<Score[1])
					System.out.println("\nLes " + games.getTeamAI() + " gagne le match apr�s des prolongations de folie!!");
				else 
					System.out.println("Les " + games.getTeamP1() + " gagne le match des suite d'un match � rallonge!!");
			}
		}
	}

	/*Affecte la bonne statistic a l'IA pour l'opposition ad�quate
	 * @since 0.2
	 * @param s Statistique d'opposition
	 * @return Statistique oppos�
	 */
	public static String CompOpp(String s) {
		String res = " ";
		switch(s) {
		case "ATT":
			res = "DEF";
			break;
		case "DEF":
			res = "ATT";
			break;
		case "ORG":
			res = "REB";
			break;
		case "REB":
			res = "ORG";
			break;
		}
		return res;
	}

	/*Affecte la bonne statistic au joueur apr�s avoir s�lectionn� sont joueur par rapport a la stat d'opposition
	 * @since 0.2
	 * @param s Statistique d'opposition
	 * @param j Joueur s�lectionn� par P1
	 * @param games Partie en cours
	 * @return i Stat pour le Joueur
	 */
	public static int Choix(String s,int j,Game games) {
		int i = 0;
		switch(s) {
		case "ATT":
			i = Player.Players(games.getTeamEffIP1(j)).getATT();
			break;
		case "DEF":
			i = Player.Players(games.getTeamEffIP1(j)).getDEF();
			break;
		case "ORG":
			i = Player.Players(games.getTeamEffIP1(j)).getORG();
			break;
		case "REB":
			i = Player.Players(games.getTeamEffIP1(j)).getREB();
			break;
		}
		return i;
	}
	/*Affecte la bonne statistic a l'AI apr�s avoir s�lectionn� sont joueur par rapport a la stat d'opposition
	 * @since 0.2
	 * @param s Statistique d'opposition
	 * @param j Joueur s�lectionn� au hasard pour AI
	 * @param games Partie en cours
	 * @return Stat pour l'AI
	 */
	public static int statAI(String s,int j,Game games) {
		int i = 0;
		switch(s) {
		case "ATT":
			i = Player.Players(games.getTeamEffIAI(j)).getATT();
			break;
		case "DEF":
			i = Player.Players(games.getTeamEffIAI(j)).getDEF();
			break;
		case "ORG":
			i = Player.Players(games.getTeamEffIAI(j)).getORG();
			break;
		case "REB":
			i = Player.Players(games.getTeamEffIAI(j)).getREB();
			break;
		}
		return i;
	}
	/*Attribut le score en fonction des stat des deux cartes
	 * @since 0.10.1
	 * @param Score Score actuel
	 * @param j Stat de P1
	 * @param i Stat de AI
	 * @return i Score apr�s l'opposition
	 */
	public static int[] Score(int[] Score,int j,int k) {
		int[] i = Score;
		int g = 25;
		int p = 15;
		int e = 17;
		if(j>k) {
			i[0] = (i[0]+g);
			i[1] = (i[1]+p);
		}else if (j<k) {
			i[0] = (i[0]+p);
			i[1] = (i[1]+g);
		}else {
			i[0] = (i[0]+e);
			i[1] = (i[1]+e);
		}
		return i;
	}
}





