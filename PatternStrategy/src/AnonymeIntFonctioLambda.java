import com.PS.comportement.*;

public class AnonymeIntFonctioLambda {

	  public static void main(String[] args) {
	    Personnage pers = new Guerrier();
	    pers.soigner();     
	    pers.setSoin(new Operation());
	    pers.soigner();
	    
	    //Utilisation d'une classe anonyme
	    pers.setSoin(new Soin() {
	    	public void soigner() {
	    		System.out.println("Je soigne avec une classe anonyme ! ");
			}
	    });
	    
	    pers.soigner();
	  }
	
}

/*Code sans lambda
 * Dialoguer d = new Dialoguer() {
 *	public void parler(String question) {
 *		System.out.println("Tu as dis : " + question);	
 *	}
 *};
 *d.parler("Bonjour");
 *
 *Code avec lambda
 *
 *Dialoguer d = (s) -> System.out.println("Tu as dis : " + s);
 *d.parler("Bonjour");
 */

