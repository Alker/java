import com.PS.comportement.*;

public abstract class Personnage {
	
	//Nos instances de comportement
	protected EspritCombatif espritCombatif = new Pacifiste();
	protected Soin soin = new AucunSoin();
	protected Deplacement deplacement = new Marcher();
	
	//Constructeur par d�faut
	public Personnage() {}
	
	//Constructeur avec param�tres
	public Personnage(EspritCombatif espritCombatif, Soin soin, Deplacement deplacement) {
		this.espritCombatif = espritCombatif;
		this.soin = soin;
		this.deplacement = deplacement;
	}
	
	//M�thode de d�placement de personnage
	public void seDeplacer() {
		//On utilise les objets de d�placement de fa�on polymorphe
		deplacement.deplacer();
	}

	//M�thode que les combattants utilisent
	public void combattre() {
		espritCombatif.combat();
	}
	
	//M�thode de soin
	public void soigner() {
		soin.soigner();
	}
	
	protected void setEspritCombatif(EspritCombatif espritCombatif) {
		this.espritCombatif = espritCombatif;
	}
	
	protected void setSoin(Soin soin) {
		this.soin = soin;
	}
	
	protected void setDeplacement(Deplacement deplacement) {
		this.deplacement = deplacement;
	}
}
