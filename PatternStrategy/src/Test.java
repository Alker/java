import com.PS.comportement.*;

class Test {

	public static void main(String[] args) {
		
		Personnage[] tPers = {new Guerrier(), new Chirurgien(), new Civil(), new Sniper(), new Medecin()};
		
		for(int i = 0; i < tPers.length; i++){
			System.out.println("\nInstance de " + tPers[i].getClass().getName());
			System.out.println("**************************************");
			tPers[i].combattre();
			tPers[i].seDeplacer();
			tPers[i].soigner();
		}
		
		Personnage pers = new Guerrier();
		System.out.println("\nInstance de " + pers.getClass().getName());
		System.out.println("**************************************");
		pers.combattre();
		pers.seDeplacer();
		pers.soigner();
		pers.setSoin(new Operation());
		System.out.println("Niveau sup: Je suis form� au op�ration!!!");
		pers.soigner();
	}
}
