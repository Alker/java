try(FileInputStream fis = new FileInputStream("test.txt"); 
FileOutputStream fos = new FileOutputStream("test2.txt")) {
  byte[] buf = new byte[8];
  int n = 0;
  while((n = fis.read(buf)) >= 0){
    fos.write(buf);       
    for(byte bit : buf)
      System.out.print("\t" + bit + "(" + (char)bit + ")");         
   
    System.out.println("");
  }

  System.out.println("Copie terminée !");
        
} catch (IOException e) {
  e.printStackTrace();
}