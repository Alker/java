import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
public class Fenetre extends JFrame implements ActionListener{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1519477616449181997L;
	private Panneau pan = new Panneau();
	private Bouton bouton = new Bouton("Go");
	private Bouton bouton2 = new Bouton("Stop");
	private JPanel container = new JPanel();
	private JLabel label = new JLabel("Mon Jlabel");
	//Compteur de clic
	private int compteur = 0;
	private boolean animated = true;
	private boolean backX, backY;
	private int x, y;

	public Fenetre(){
		this.setTitle("Animation");
		this.setSize(300, 300);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);

		container.setBackground(Color.white);
		container.setLayout(new BorderLayout());
		container.add(pan, BorderLayout.CENTER);

		//Ce sont maintenan nos classes internes qui �coutent nos boutons
		bouton.addActionListener(new BoutonListener());
		bouton2.addActionListener(new Bouton2Listener());

		JPanel south = new JPanel();
		south.add(bouton);
		south.add(bouton2);
		container.add(south, BorderLayout.SOUTH);

		//D�finition d'une police d'�criture
		Font police = new Font("Comic Sans MS", Font.BOLD, 16);
		//On l'applique au JLabel
		label.setFont(police);
		//Changement de la couleur du texte
		label.setForeground(Color.blue);
		//On modifie l'alignement du texte gr�ce aux attribut statiques de JLabel
		label.setHorizontalAlignment(JLabel.CENTER);

		container.add(label, BorderLayout.NORTH);
		this.setContentPane(container);
		this.setVisible(true);    
		go();
	}

	//M�thode qui sera appel�e lors d'un clic sur le bouton
	public void actionPerformed(ActionEvent arg0) {
		if(arg0.getSource() == bouton)
			label.setText("Vous avez cliqu� sur le bouton 1");
		if(arg0.getSource() == bouton2)
			label.setText("Vous avez cliqu� sur le bouton 2");
	}

	private void go(){  
		//Les coordonn�es de d�part de notre rond
		x = pan.getPosX(); 
		y = pan.getPosY();
		//Dans cet exemple, j'utilise une boucle while
		//Vous verrez qu'elle fonctionne tr�s bien
		while(this.animated){
			if(x < 1)backX = false;
			if(x > pan.getWidth()-50)backX = true;
			if(y < 1)backY = false;
			if(y > pan.getHeight()-50)backY = true;
			if(!backX)
				pan.setPosX(++x);
			else
				pan.setPosX(--x);
			if(!backY)
				pan.setPosY(++y);
			else pan.setPosY(--y);
			pan.repaint();
			
			try {
				Thread.sleep(3);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}    
	}
	class BoutonListener implements ActionListener{
		public void actionPerformed(ActionEvent arg0) {
			animated = true;
			bouton.setEnabled(false);
			bouton2.setEnabled(true);
		}
	}
	//Classe �coutant le bouton 2
	class Bouton2Listener implements ActionListener{
		public void actionPerformed(ActionEvent arg0) {
			animated = false;
			bouton.setEnabled(true);
			bouton2.setEnabled(false);
		}
	}
}