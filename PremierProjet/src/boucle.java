// Boucle while
import java.util.Scanner;

public class boucle {
	public static void main(String[] args) {
		String prenom;
		
		char reponse = 'O';
		
		Scanner sc = new Scanner(System.in);
		
		while(reponse == 'O')
		{
			System.out.println("Donnez un pr�nom : ");
			
			prenom = sc.nextLine();
			
			System.out.println("Bonjour " +prenom+ ", comment vas_tu ?");
			
			reponse = ' ';
			
			while(reponse != 'O' && reponse != 'N')
			{
				
			System.out.println("Voulez-vous r�essayer ? (O/N)");	
			reponse = sc.nextLine().charAt(0);
			}
			
		}
		System.out.println("Au revoir...");
		sc.close();
	}

}
