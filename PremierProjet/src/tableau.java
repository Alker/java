//Tableau 1 dimension
public class tableau {
	public static void main(String[] args) {
		//tableau d�j� rempli
		int tableauEntier[] = {0,1,2,3,4,5,6,7,8,9};
		double tableauDouble[] = {0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0};
		char tableauCaractere[] = {'a','b','c','d','e','f','g'};
		String tableauChaine[] = {"chaine1","chaine2","chaine3","chaine4"};
		//Tableau vide
		int tableauVideEntier[] = new int[6];
		//ou encore
		int[] tableauVideEntier2 = new int[6];

		//affichage du tableau
		for(int i=0; i < tableauCaractere.length; i++)
			System.out.println("A l'emplacement " + i + " du tableau nous avons = " + tableauCaractere[i]);

	}
}
