
public class methodeClasse {
	public static void main(String[] args) {
		// vers minuscule
		String chaine = new String("SALUT TOUT LE MONDE !"), chaine2 = new String();
		chaine2 = chaine.toLowerCase();// Donne "coucou tout le monde !"
		//vers MAJUSCULE
		String chaine3 = new String("coucou coucou"), chaine4 = new String();
		chaine4 = chaine3.toUpperCase(); // Donne "COUCOU COUCOU"
		//Longueur de la cha�ne
		String chaine5 = new String("coucou ! ");
		int longueur = 0;
		longueur = chaine.length(); // Renvoi 9
		//verification identique ou non
		String str1 = new String("coucou"), str2 = new String("toutou");
		
		if(str1.equals(str2))
			System.out.println("Les deux cha�nes sont identiques !");
		
		else
			System.out.println("Les deux cha�nes sont diff�rentes !");
		// Ici avec l'in�galit� ( op�rateur !)
		if(!str1.equals(str2))
			System.out.println("Les deux cha�nes sont diff�rentes !");
		
		else
			System.out.println("Les deux cha�nes sont identiques !");
		//Extraction de caract�re
		String nbre = new String("12345678");
		char carac = nbre.charAt(4); // Renverra ici le caract�re 5
		//extraction d'une partie de caract�re
		String chaine6 = new String("la paix niche"), chaine7 = new String();
		chaine7 = chaine6.substring(3,13); // Permet d'extraire "Paix niche"
		//Renvoi la position(index)
		//.indexOf() explore du d�but
		//.lastIndexOf() explore de la fin
		String mot = new String("anticonstitutionnellement");
		int n = 0;
		
		n = mot.indexOf('t');		//n vaut 2
		n = mot.lastIndexOf('t');	//n vaut 24
		n = mot.indexOf("ti");		//n vaut 2
		n = mot.lastIndexOf("ti");	//n vaut 12
		n = mot.indexOf('x');		//n vaut -1
	}
}
