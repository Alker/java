import java.util.Scanner;
// Ceci importe la classe Scanner du package java.util
//import java.util.*;


public class premierClasse {
	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		/* Int, double long et byte sont pris en charge par Scanner, sauf une seul, Char, ne l'est pas
		 * pour Char il faut procedez ainsi: char carac = str.charAt(0);*/
		System.out.println("Veuillez saisir un nombre :");
		int str = sc.nextInt();
		System.out.println("Vous avez saisi  le nombre : " + str);
		sc.close();
	
		
	}
	// \n -> ajout retour � la ligne
	// println()  -> texte puis retour � la ligne
	// print() -> texte sans retour � la ligne
	// \t -> fait une tabulation
	// \r -> fait un retour chariot, parfois utilis� comme saut de ligne
	// pour afficher les " il faut mettre \ devant.
	
}
