import java.util.Scanner;

public class TPcelsiusFahren {
	public static void main(String[] args) {
		int choix = 0, choix2 = 0;
		double celsius, fahren, arrondC, arrondF;
		char reponse = 'O';

		Scanner sc = new Scanner(System.in);

		System.out.println("CONVERTISSEUR DEGR�S CELSIUS ET DEGR�S FAHRENHEIT");
		System.out.println("-------------------------------------------------");
		do
		{
			reponse =  ' ';
			do
			{
				System.out.println("Choisissez le mode de conversion :");
				System.out.println("1 - Convertisseur Celsius - Fahrenheit ");
				System.out.println("2 - Convertisseur Fahrenheit - Celsius ");
				choix = sc.nextInt();

				if(choix != 1 && choix != 2)
					System.out.println("Mode inconnu");
			}while(choix != 1 && choix != 2);

			if (choix == 1) {
				while (choix2 != 1 && choix2 !=2)
				{
					System.out.println("1 - Convertisseur Pr�cis ");
					System.out.println("2 - Convertisseur Arrondi ");
					choix2 = sc.nextInt();
				}
				if(choix2 == 1) {
					System.out.println("Temp�rature � convertir :(Attention utiliser la virgule et non le point)");
					celsius = sc.nextDouble();
					fahren = ( 9.0 / 5.0 ) * celsius + 32.0;
					System.out.println(celsius + "�C correspond � : " + fahren + "�F.");
				}else{
					System.out.println("Temp�rature � convertir :");
					celsius = sc.nextDouble();
					fahren = ( 9.0 / 5.0 ) * celsius + 32.0;
					arrondC = arrondi(celsius,1);
					arrondF = arrondi(fahren,1);
					System.out.println(arrondC + "�C correspond � : " + arrondF + "�F.");
				}
			}
			else {
				while (choix2 != 1 && choix2 !=2)
				{
					System.out.println("1 - Convertisseur Pr�cis ");
					System.out.println("2 - Convertisseur Arrondi ");
					choix2 = sc.nextInt();
				}
				if(choix2 == 1) {
					System.out.println("Temp�rature � convertir :");
					fahren = sc.nextDouble();
					celsius = ((fahren-32.0)*5.0)/9.0;
					System.out.println(fahren + "�F correspond � : " + celsius + "�C.");
				}else{
					System.out.println("Temp�rature � convertir :(Attention utiliser la virgule et non le point)");
					fahren = sc.nextDouble();
					celsius = ((fahren-32.0)*5.0)/9.0;
					arrondC = arrondi(celsius,1);
					arrondF = arrondi(fahren,1);
					System.out.println(arrondF + "�F correspond � : " + arrondC + "�C.");
				}

			}
			do {
				System.out.println("Voulez-vous r�essayer ? (O/N)");	
				reponse = sc.next().charAt(0);
				choix2 = 3;
			}while(reponse != 'O' && reponse != 'N');
		}while(reponse == 'O');
		System.out.println("Merci, au revoir");
		sc.close();
	}

	public static double arrondi(double A, int B) {
		return (double) ( (int) (A *  Math.pow(10, B) + .5)) / Math.pow(10, B); 
	}

}
