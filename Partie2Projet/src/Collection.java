/*import java.util.List;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;*/
import java.util.HashSet;
import java.util.Iterator;
public class Collection {

	public static void main(String[] args) {
		/*List l = new LinkedList();
		l.add(12);
		l.add("Toto !");
		l.add(12.20f);
		
		for(int i = 0;i < l.size(); i++)
			System.out.println("Element � l'index " + i + " = " + l.get(i));
		
		
		System.out.println("\n \tParcours avec un it�rateur");
		System.out.println("----------------------------");
		ListIterator li = l.listIterator();
		
		while(li.hasNext())
			System.out.println(li.next());
		ArrayList al = new ArrayList();
		al.add(12);
		al.add("Une chaine de caract�re");
		al.add(12.20f);
		al.add('d');
		
		for(int i = 0;i < al.size(); i++)
			System.out.println("Donn�e � l'indice " + i + " = " + al.get(i));
			/*M�thode array list
			 *  add()  ajout �l�ment
			 *  get(int i) retourne l'�l�ment en i
			 *  remove(int i) supprime l'�l�ment en ii
			 *  isEmpty() vrai si l'objet est vide
			 *  removeAll() efface tout le contenu de l'objet
			 *  contains(Object element) retourne "vrai" si l'�l�ment pass� en param�tre est dans l'arraylist
			 *//*
		Hashtable ht = new Hashtable();
		ht.put(1, "printemps");
		ht.put(10, "�t�");
		ht.put(20, "automne");
		ht.put(48, "hiver");
		
		
		Enumeration e = ht.elements();
		
		while(e.hasMoreElements())
			System.out.println(e.nextElement());
		/*Panel de methode Hashtable
		 * isEmpty() retourne � vrai � si l'objet est vide ;
		 * 
		 * contains(Object value) retourne � vrai � si la valeur est pr�sente. 
		 * Identique � containsValue(Object value) ;
		 * 
		 * containsKey(Object key) retourne � vrai � si la cl� pass�e en param�tre 
		 * est pr�sente dans la Hashtable ;
		 * 
		 * put(Object key, Object value) ajoute le couple key - value dans l'objet ;
		 *
		 * elements() retourne une �num�ration des �l�ments de l'objet ;
		 * 
		 * keys() retourne la liste des cl�s sous forme d'�num�ration.
		 */
		
		HashSet hs = new HashSet();
		hs.add("toto");
		hs.add(12);
		hs.add('d');
		
		
	Iterator it = hs.iterator();
	while(it.hasNext())
		System.out.println(it.next());
	
	System.out.println("\nParcours avec un tableau d'objet");
	System.out.println("---------------------------------");
	
	Object[] obj = hs.toArray();
	for(Object o : obj)
		System.out.println(o);
	
	/* add() ajoute un �l�ment ;
	 * contains(Object value) retourne � vrai � si l'objet contient value ;
	 * isEmpty() retourne � vrai � si l'objet est vide ;
	 * iterator() renvoie un objet de type Iterator ;
	 * remove(Object o) retire l'objet o de la collection ;
	 * toArray() retourne un tableau d'Object.
	 */
	}
}
