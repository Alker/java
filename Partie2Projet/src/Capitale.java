
public class Capitale extends Ville {
	
	private String monument;
	//Constructeur par d�faut
	public Capitale() throws NombreHabitantException, NomVilleException{
		//Ce mot cl� appelle le constructeur de la classe m�re
		super();
		monument = "aucun";
	}
	
	public Capitale(String nom, int hab, String pays, String monument) throws NombreHabitantException, NomVilleException{
		super(nom, hab, pays);
		this.monument = monument;
	}
	
	/**Description d'une capitale
	 * @return String retourne la description de l'objet
	 */
	
	public String decrisToi() {
		String str = super.decrisToi() + "\n \t==>>" + this.monument+ " en est un monument.";
		System.out.println("Invocation de super.decrisToi()");
		return str;
	}
	
	/**
	 * @return le nom du monument
	 */
	public String getMonument() {
		return monument;
	}

	//D�finie  le nom du monument
	public void setMonument(String monument) {
		this.monument = monument;
	}
	public String toString() {
		String str = super.toString() + " \n \t ==>>" + this.monument + " en est un monument";
		return str;
	}
}
