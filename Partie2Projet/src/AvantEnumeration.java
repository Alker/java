
public class AvantEnumeration {
	
	public void fait(Language param) {
		if(param.equals(Language.JAVA))
			System.out.println("Fait de la fa�on N�1");
		if(param.equals(Language.PHP))
			System.out.println("Fait de la fa�on N�2");
	}
	
	
	public static void main(String[] args) {
		AvantEnumeration ae = new AvantEnumeration();
		ae.fait(Language.JAVA);
		ae.fait(Language.PHP);
	}
}
