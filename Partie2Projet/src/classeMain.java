public class classeMain {
	public static void main(String[] args) throws NombreHabitantException, NomVilleException {
		/*Ville ville = new Ville();
		System.out.println("Le nombre d'instances de la classe Ville est : " + Ville.nbreInstances + "(Variable publique)\n");
		System.out.println("Le nombre d'instances de la classe Ville est : " + Ville.getNombresInstancesBis() + "(Variable priv�)\n");

		Ville reims = new Ville("Reims", 184076,"France");
		System.out.println("Le nombre d'instances de la classe Ville est : " + Ville.nbreInstances + "(Variable publique)\n");
		System.out.println("Le nombre d'instances de la classe Ville est : " + Ville.getNombresInstancesBis() + "(Variable priv�)\n");

		Ville lyon = new Ville("Lyon", 187526,"France");
		System.out.println("Le nombre d'instances de la classe Ville est : " + Ville.nbreInstances + "(Variable publique)\n");
		System.out.println("Le nombre d'instances de la classe Ville est : " + Ville.getNombresInstancesBis() + "(Variable priv�)\n");

		System.out.println("test des ACCESSEURS");
		System.out.println(reims.getNomVille() + " ville de "+ reims.getNombresHab() + " habitants se situant en " + reims.getNomPays() + "\n");
		System.out.println(lyon.getNomVille() + " ville de "+ lyon.getNombresHab() + " habitants se situant en " + lyon.getNomPays() + "\n");
		System.out.println("*******************");
		System.out.println("test des MUTATEURS");
		Ville auberive = new Ville();
		auberive.setNomVille("Aub�rive");
		auberive.setNombreHabitants(214);
		auberive.setNomPays("France");
		System.out.println("Le nombre d'instances de la classe Ville est : " + Ville.nbreInstances + "(Variable publique)\n");
		System.out.println("Le nombre d'instances de la classe Ville est : " + Ville.getNombresInstancesBis() + "(Variable priv�)\n");


		System.out.println(reims.getNomVille() + " ville de "+ reims.getNombresHab() + " habitants se situant en " + reims.getNomPays() + "\n");
		System.out.println(lyon.getNomVille() + " ville de "+ lyon.getNombresHab() + " habitants se situant en " + lyon.getNomPays() + "\n");
		System.out.println(auberive.getNomVille() + " ville de "+ auberive.getNombresHab() + " habitants se situant en " + auberive.getNomPays() + "\n");

		System.out.println("Comparaison des villes");

		System.out.println(reims.comparer(auberive) + "\n");
		System.out.println(lyon.comparer(reims));
		System.out.println(lyon.comparer(auberive) + "\n");
		System.out.println(auberive.comparer(reims) + "\n");
		System.out.println(auberive.comparer(lyon) + "\n");

		System.out.println("Comparaison des villes");

		System.out.println(reims.decrisToi() + "\n");
		System.out.println(lyon.decrisToi() + "\n");
		System.out.println(auberive.decrisToi() + "\n");

		Capitale cap = new Capitale("Paris", 2187526, "France", "La Tour Effeil");
		System.out.println("\n"+cap.decrisToi());*/

		//D�finition d'un tableau de villes null
		Ville[] tableau = new Ville[6];

		//D�finition d'un tableau de noms de villes et un autre de nombres d'habitants
		String[] tab = {"Marseille", "Lille", "Caen", "Lyon", "Paris", "Nantes"};
		int[] tab2 = {123456, 78456, 654987, 75832165, 1594, 213};
		String[]tab3 = {"Le Vieux Port", "La vieille Bourse ", "Chapelle Sainte-Paix", "La Basilique Notre Dame de Fourvi�re", "La Tour Eiffel", "Le Monument au 50 otages"};

		//Les trois premiers �l�ments du tableau seront des villes et le reste des capitales
		for(int i = 0; i< 6; i++) {
			if(i<3) {
				Ville V = null;
				try {
					V = new Ville(tab[i], tab2[i],"france");
					tableau[i] = V;
				}catch(NombreHabitantException e) {}
			}
			else {
				Capitale C = null;
				try {
					C = new Capitale(tab[i], tab2[i], "france", tab3[i]);
					tableau[i] = C;
				}catch (NombreHabitantException e) {}}
		}
		//Il ne nous reste plus qu'� d�crire tout notre tableau!
		for(Object obj : tableau) {
			System.out.println(obj.toString()+"\n");
		}
	}
}
