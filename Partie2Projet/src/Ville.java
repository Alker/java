
public class Ville {
	//Stocke le nom de notre ville
		protected String nomVille;
	//Stocke le nom du pays de notre ville
		protected String nomPays;
	//Stocke le nombre d'habitant de notre ville
		protected int nbreHabitants;
	//Stocke la cat�gorie de la Ville
		protected char categorie;
	//Variable public qui compte les instances
		public static int nbreInstances = 0;
	//Variable priv�e qui comptera aussi les instances
		protected static int nbreInstancesBis = 0;
		
		//Constructeur par d�faut
		public Ville() {
			//On incr�mente nos variables � chaque appel aux constructeurs
			nbreInstances++;
			nbreInstancesBis++;
			System.out.println("Cr�ation d'une ville !");
			nomVille = "Inconnu";
			nomPays = "Inconnu";
			nbreHabitants = 0;
			this.setCategorie();
		}
		//Constructeur avec param�tres, surcharge
		public Ville(String nom, int pop, String nomP) throws NombreHabitantException, NomVilleException
		{
			if(pop < 0)
				throw new NombreHabitantException(pop);
			if(nom.length() < 3)
				throw new NomVilleException("Le nom de la ville est inf�rieur � 3 caract�res ! nom = " + nom);
			else {
				//On incr�mente nos variables � chaque appel aux constructeurs
				nbreInstances++;
				nbreInstancesBis++;
				System.out.println("Cr�ation de la ville "+ nom + "!");
				nomVille = nom;
				nomPays = nomP;
				nbreHabitants = pop;
				this.setCategorie();
			}
		}
		
	//***************ACCESSEURS******************
		
		//Retourne le nom de la Ville
		public String getNomVille() {
			return nomVille;
		}

		//Retourne le nom du Pays
		public String getNomPays() {
			return nomPays;
		}
		
		//Retourne le nombres d'habitant
		public int getNombresHab() {
			return nbreHabitants;
		}
		
		//retourne la cat�gorie de la ville
		public char getCategorie() {
			return categorie;
		}
		
		//Retourne le nombre d'instance de la variable priv�
		public static int getNombresInstancesBis() {
			return nbreInstancesBis;
		}
		
	//**************MUTATEURS********************
		
		//D�finit le nom de la ville
		public void setNomVille(String nomV) {
			nomVille =nomV;
		}
		
		//D�finit le nom du Pays
		public void setNomPays(String nomP) {
			nomPays =nomP;
		}
		
		//D�finit le nombres d'habitant
		public void setNombreHabitants(int nbre) {
			nbreHabitants =nbre;
		}
		
		//D�finit la cat�gorie de la ville
		public void setCategorie() {
			
			int bornesSuperieures[] = {0, 1000, 10000, 100000 ,500000 ,1000000, 5000000, 100000000};
			char categories[] = {'?', 'A', 'B', 'C', 'D', 'E', 'F', 'G'};
			
			int i = 0;
			while (i < bornesSuperieures.length && this.nbreHabitants > bornesSuperieures[i])
				i++;
			
			this.categorie = categories[i];
		}
		
		//Retourne la description de la Ville
		public String decrisToi() {
			return "\t"+this.nomVille+" est une ville de "+this.nomPays+", elle comporte : "+this.nbreHabitants+" habitant(s) => elle est de cat�gorie : "+this.categorie;
		}

		//Retourne une cha�ne de caract�re selon le r�sultat de la comparaison
		public String comparer(Ville v1) {
			String str = new String();
			
			if(v1.getNombresHab() > this.nbreHabitants)
				str = v1.getNomVille() + " est une ville plus peupl� que " + this.nomVille;
			else
				str = this.nomVille + " est une ville plus peupl� que " + v1.getNomVille();
			return str;
		}
		public String toString() {
			return "\t"+this.nomVille+" est une ville de "+this.nomPays+", elle comporte : "+this.nbreHabitants+" => elle est donc de cat�gorie : "+this.categorie;
		}
		
}
