package fr.chapitrepackage2.pkg;
import fr.chapitrepackage.pkg.A;
import fr.chapitrepackage.pkg.B;
// Probl�me car Classe B est en Default et non en public, pour r�soudre, il faut passer B en public, seule solution.

public class Main {
	public static void main(String[] args) {
		A a = new A();
		B b = new B();
	}
}
