public class Main2 {
	public static void main(String[] args) {
		Ville v = null;
		try {
			v = new Ville("Reims", -12000, "France");
		}
		//Gestion de plusieurs exceptions différentes
		catch (NombreHabitantException | NomVilleException e) {
			System.out.println(e.getMessage());
		}
		finally {
			if(v == null)
				v = new Ville();
		}
		System.out.println(v.toString());
	}
}
