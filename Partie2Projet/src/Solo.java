
public class Solo<T> {
	
	//Variable d'instance
	private T valeur;
	//Constructeur par defaut
	public Solo() {
		this.valeur = null;
	}
	//Constructeur avec param�tre inconnu pour l'instant
	public Solo(T val) {
		this.valeur = val;
	}
	//Retourne la valeur d�j� "caster" par la signature de la m�thode
	public T GetValeur() {
		return this.valeur;
	}
	//D�finit la valeur avec le param�tre
	public void SetValeur(T val) {
		this.valeur = val;
	}
}
