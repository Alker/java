
public enum Language {
	JAVA ("Language JAVA", "Eclipse"),
	C ("Language C", "Code Block"),
	CPlus ("Language C+", "Visualstudio"),
	PHP ("Language PHP", "PS Pad");
	
	private String name = "";
	private String editor = "";
	
	Language(String name, String editor){
		this.name = name;
		this.editor = editor;
	}
	
	public void getEditor() {
		System.out.println("Editeur : " + editor);
	}
	public String toString() {
		return name;
	}
	
	public static void main(String[] args) {
		Language l1 = Language.JAVA;
		Language l2 = Language.PHP;
	
		l1.getEditor();
		l2.getEditor();
	}
}
